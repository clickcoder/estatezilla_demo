--
-- Dumping data for table `locales`
--

INSERT INTO `locales` VALUES
(1, 'en', 'English', 0, 1, '2015-08-07 14:41:27', '2015-08-07 14:41:27');

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` VALUES
(1, 'Main menu', 'main', '{"en":[{"id":1,"title":"Home","nodes":[],"type":"page","value":{"id":1,"title":"About us"}},{"id":8,"title":"For sale","nodes":[],"type":"page","value":{"id":3,"title":"For sale","slug":"for-sale"}},{"id":10,"title":"To rent","nodes":[],"type":"page","value":{"id":4,"title":"For rent","slug":"for-rent"}},{"id":14,"title":"About","nodes":[],"type":"page","value":{"id":5,"title":"About us","slug":"about-us"}},{"id":5,"title":"Contact","nodes":[],"type":"page","value":{"id":8,"title":"Contact us","slug":"contact-us"}},{"id":2,"title":"Map of properties","nodes":[],"type":"page","value":{"id":2,"title":"Map","slug":"map"}}]}', '2015-08-07 14:41:27', '2015-08-08 17:02:58'),
(2, 'Footer menu', 'footer', '{"en":[{"id":7,"title":"Home","nodes":[],"type":"page","value":{"id":1,"title":"Home","slug":""}},{"id":1,"title":"About","nodes":[],"type":"page","value":{"id":5,"title":"About us","slug":"about-us"}},{"id":3,"title":"Terms and Conditions","nodes":[],"type":"page","value":{"id":6,"title":"Terms and conditions","slug":"terms-and-conditions"}},{"id":5,"title":"Contact us","nodes":[],"type":"page","value":{"id":8,"title":"Contact us","slug":"contact-us"}}]}', '2015-08-07 14:41:27', '2015-08-09 15:02:25');

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` VALUES
(1, 'Home', 'home', NULL, '', NULL, '2015-08-07 21:54:05', '2015-08-08 17:48:37'),
(2, 'Map', 'map', NULL, '', NULL, '2015-08-07 22:15:04', '2015-08-07 22:15:04'),
(3, 'for-sale', 'listings', NULL, '', NULL, '2015-08-08 08:55:36', '2015-08-08 08:55:36'),
(4, 'For rent', 'listings', NULL, '', NULL, '2015-08-08 14:41:44', '2015-08-08 14:41:44'),
(5, 'About us', 'pages.plain-page', NULL, '', NULL, '2015-08-08 14:44:39', '2015-08-08 14:44:39'),
(6, 'Terms and conditions', 'pages.plain-page', NULL, '', NULL, '2015-08-08 14:45:09', '2015-08-08 14:45:09'),
(7, 'Privacy policy', 'pages.plain-page', NULL, '', NULL, '2015-08-08 14:45:38', '2015-08-08 14:45:38'),
(8, 'Contact us', 'pages.contact-us', NULL, '', NULL, '2015-08-08 14:46:26', '2015-08-08 14:46:26'),
(9, 'Listings', 'listings', NULL, 'listings', NULL, '2015-08-09 09:02:27', '2015-08-09 09:03:40');

--
-- Dumping data for table `page_translations`
--

INSERT INTO `page_translations` VALUES
(1, 1, 'en', 'Home', '', NULL, 'Home', NULL, NULL, 'DRAFT', 'VISIBLE', '0000-00-00 00:00:00', '2015-08-07 21:54:05', '2015-08-09 15:55:03', NULL),
(2, 2, 'en', 'Map', 'map', NULL, 'Property map', NULL, NULL, 'DRAFT', 'VISIBLE', '0000-00-00 00:00:00', '2015-08-07 22:15:05', '2015-08-08 17:22:46', NULL),
(3, 3, 'en', 'For sale', 'for-sale', NULL, 'For sale', 'For sale meta', 'houses for sale, yes', 'DRAFT', 'VISIBLE', '0000-00-00 00:00:00', '2015-08-08 08:55:36', '2015-08-08 14:32:09', NULL),
(4, 4, 'en', 'For rent', 'for-rent', NULL, NULL, NULL, NULL, 'DRAFT', 'VISIBLE', '0000-00-00 00:00:00', '2015-08-08 14:41:44', '2015-08-08 17:47:58', NULL),
(5, 5, 'en', 'About us', 'about-us', '<p>Enter your about us text here</p>\n', NULL, NULL, NULL, 'DRAFT', 'VISIBLE', '0000-00-00 00:00:00', '2015-08-08 14:44:39', '2015-08-08 16:54:03', NULL),
(6, 6, 'en', 'Terms and conditions', 'terms-and-conditions', '<p>Enter your terms and conditions here</p>\n', NULL, NULL, NULL, 'DRAFT', 'VISIBLE', '0000-00-00 00:00:00', '2015-08-08 14:45:09', '2015-08-09 15:02:47', NULL),
(7, 7, 'en', 'Privacy policy', 'privacy-policy', '<p>Enter your privacy policy here</p>\n', NULL, NULL, NULL, 'DRAFT', 'VISIBLE', '0000-00-00 00:00:00', '2015-08-08 14:45:38', '2015-08-09 15:03:03', NULL),
(8, 8, 'en', 'Contact us', 'contact-us', '<h3>Our headquarters</h3>\n\n<p>Our offices are open from 9am - 8pm Monday to Friday and 9am - 5pm on Saturdays, Sundays and Bank Holidays. We&#39;re open 7 days a week. We aim to reply to your queries as soon as possible.</p>\n\n<p>Phone number : XXX-XXX-XXX</p>\n\n<p>Contact us today if you have any queries for any of the following :</p>\n\n<ul>\n	<li>Complaints</li>\n	<li>Feedback or suggestions</li>\n	<li>Website problem</li>\n	<li>Listing your property</li>\n	<li>anything else...</li>\n</ul>\n', NULL, NULL, NULL, 'DRAFT', 'VISIBLE', '0000-00-00 00:00:00', '2015-08-08 14:46:26', '2015-08-08 17:08:34', NULL),
(9, 9, 'en', 'Listings', 'listings', NULL, 'Listings', NULL, NULL, 'DRAFT', 'VISIBLE', '0000-00-00 00:00:00', '2015-08-09 09:02:27', '2015-08-09 09:03:40', NULL);

--
-- Dumping data for table `properties`
--

INSERT INTO `properties` VALUES
(1, 75, 1, 'sale', 'for_sale', 'new', NULL, 6, 3, 2, 2, 1, 1, 0, NULL, '[{"file":"0.jpg","caption":"Photo 1"},{"file":"1.jpg","caption":"Photo 2"},{"file":"2.jpg","caption":"Photo 3"},{"file":"3.jpg","caption":"Photo 4"},{"file":"4.jpg","caption":"Photo 5"},{"file":"5.jpg","caption":"Photo 6"},{"file":"6.jpg","caption":"Photo 7"},{"file":"7.jpg","caption":"Photo 8"},{"file":"8.jpg","caption":"Photo 9"}]', NULL, NULL, NULL, 'Wilton Mews, London, SW1X', '1', 'Groom Place', 'London', 'Greater London', 'SW1X 7BA', 'GB', '51.498955348381', '-0.151036027427', '42500000.00', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '48167653', 1, 0, '2015-07-16 22:09:04', '2015-07-18 06:37:36', NULL);

--
-- Dumping data for table `property_translations`
--

INSERT INTO `property_translations` VALUES
(1, 1, 'en', 'A demo property', 'A stunning family residence located in the heart of Belgravia, just off Belgrave Square.<br /><br /><b>Description</b><br /><br />The house has been interior designed and project managed by the renowned prime central London firm, Finchatton.  The property has impressive lateral space, with large reception rooms which are ideal for entertaining, together with generous bedroom accommodation with en suite facilities.  It has an indoor swimming pool, gymnasium, passenger lift serving all floors and garaging.<br /><br />Square Footage: 9803 sq ft', 'A stunning family residence located in the heart of Belgravia, just off Belgrave Square.', '["Double volume entrance hall","Kitchen and breakfast room","Dining room and 2 Drawing rooms","Study","Cinema","Master bedroom with en suite bathroom and dressing room","Guest bedroom with en suite shower room  and 4 further bedrooms with en suites","Wine cellar, Gym and Swimming pool","Roof terrace","EPC Rating = C"]', NULL, NULL, NULL, NULL, 'Wilton Mews, London, SW1X - Savills', 'A stunning family residence located in the heart of Belgravia, just off Belgrave Square.', NULL, '2015-03-12 19:31:49', '0000-00-00 00:00:00');

--
-- Dumping data for table `property_types`
--

INSERT INTO `property_types` VALUES
(1, 1, 'Detached house', 'detached-house', 1, '2015-08-10 08:30:25', '2015-08-10 08:30:25'),
(2, 1, 'Semi-detached house', 'semi-detached-house', 2, '2015-08-10 08:31:06', '2015-08-10 08:31:06'),
(3, 1, 'Terraced house', 'terraced-house', 3, '2015-08-10 08:31:11', '2015-08-10 08:31:11'),
(4, 1, 'Mobile / Park Home', 'mobile-park-home', 4, '2015-08-10 08:31:29', '2015-08-10 08:31:29'),
(5, 3, 'Bungalow', 'bungalow', 5, '2015-08-10 08:32:15', '2015-08-10 08:32:15'),
(6, 4, 'Land', 'land', 6, '2015-08-10 08:32:22', '2015-08-10 08:32:22'),
(7, 2, 'Studio', 'studio', 7, '2015-08-10 08:34:04', '2015-08-10 08:34:04'),
(8, 2, 'Flat', 'flat', 8, '2015-08-10 08:34:15', '2015-08-10 08:34:15'),
(9, 2, 'Block of flats', 'block-of-flats', 9, '2015-08-10 08:34:26', '2015-08-10 08:34:26'),
(10, 3, 'Cottage', 'cottage', 10, '2015-08-10 08:34:43', '2015-08-10 08:34:43'),
(11, 2, 'Maisonette', 'maisonette', 11, '2015-08-10 08:37:06', '2015-08-10 08:37:06'),
(12, 5, 'Office', 'office', 12, '2015-08-10 08:37:26', '2015-08-10 08:37:26'),
(13, 6, 'Other', 'other', 13, '2015-08-10 08:37:34', '2015-08-10 08:39:53'),
(14, 5, 'Retail premise', 'retail-premise', 14, '2015-08-10 08:39:43', '2015-08-10 08:39:43');

--
-- Dumping data for table `regions`
--

INSERT INTO `regions` VALUES
(1, 2, 'Map coordinates', 'map-coordinates', 'map-coordinates', '2015-08-07 22:30:29', '2015-08-08 17:19:42', '2015-08-08 17:19:42'),
(2, 2, 'Listing settings', 'listings', 'listings', '2015-08-07 22:30:29', '2015-08-07 22:30:29', NULL),
(3, 3, 'Listing settings', 'listings', 'listings', '2015-08-08 08:55:41', '2015-08-08 08:55:41', NULL),
(4, 8, 'Contact form', 'contact', 'contact', '2015-08-08 14:46:30', '2015-08-08 14:46:30', NULL),
(5, 8, 'Slideshow', 'slideshow', 'slideshow', '2015-08-08 14:46:30', '2015-08-08 14:46:30', NULL),
(6, 2, 'Map coordinates', 'map', 'map', '2015-08-08 17:19:42', '2015-08-08 17:19:42', NULL),
(7, 4, 'Listing settings', 'listings', 'listings', '2015-08-08 17:47:34', '2015-08-08 17:47:34', NULL),
(8, 1, 'Home blocks', 'home-blocks', 'home-blocks', '2015-08-08 17:48:44', '2015-08-08 18:50:11', '2015-08-08 18:50:11'),
(9, 1, 'Slideshow', 'slideshow', 'slideshow', '2015-08-08 17:48:44', '2015-08-08 17:48:44', NULL),
(10, 1, 'Listing settings', 'listings', 'listings', '2015-08-08 18:39:17', '2015-08-08 18:39:17', NULL),
(11, 1, 'Home data', 'home-data', 'home-data', '2015-08-08 18:50:11', '2015-08-08 19:54:56', '2015-08-08 19:54:56'),
(12, 1, 'Home data', 'home_data', 'home_data', '2015-08-08 19:54:56', '2015-08-08 19:54:56', NULL),
(13, 9, 'Listing settings', 'listings', 'listings', '2015-08-09 09:03:33', '2015-08-09 09:03:33', NULL);

--
-- Dumping data for table `region_translations`
--

INSERT INTO `region_translations` VALUES
(1, 6, 'en', '{"lat":"51.502584","lng":"-0.15246","zoom":"14"}', '2015-08-08 17:19:52', '2015-08-08 17:33:10'),
(2, 2, 'en', NULL, '2015-08-08 17:43:31', '2015-08-08 17:43:31'),
(3, 3, 'en', '{"listing_type":"sale","is_featured":true}', '2015-08-08 17:43:50', '2015-08-08 17:47:26'),
(4, 7, 'en', '{"listing_type":"rent","is_featured":false}', '2015-08-08 17:47:37', '2015-08-08 17:47:44'),
(5, 8, 'en', NULL, '2015-08-08 18:39:22', '2015-08-08 18:39:22'),
(6, 11, 'en', '{"heading1":"Latest properties","slogan1":"We understand what you need and what you like. Pick from our wide variety of homes.","links":[{"title":"Title 1"},{"title":"Title 2"},{"title":"Title 3"}]}', '2015-08-08 18:50:17', '2015-08-08 18:58:59'),
(7, 12, 'en', '{"heading1":"Latest properties","slogan1":"We understand what you need and what you like. Pick from our wide variety of homes.","quote_text":"Our mission statement is just to keep you happy","quote_author":"Yours sincerely, <cite title=\\"Source Title\\">Team EstateZilla<\\/cite>","testimonial_text":"I''d like to thank you for the great service provided. You helped me incredibly when selling my house and did it quickly too!","testimonial_author":"Mr. Smith in <cite title=\\"Source Title\\">the City of London<\\/cite>","links":[{"title":"Find your ideal property","caption":"Looking to buy your own property?","url":"\\/for-sale"},{"title":"Rent a place","caption":"Looking for an apartment or studio to rent?","url":"\\/to-rent"},{"title":"How can we help you?","caption":"Contact us today whether you want to buy, sell or rent a property","url":"\\/contact-us"}]}', '2015-08-08 19:55:03', '2015-08-08 21:59:25'),
(8, 9, 'en', '[{"title":"Map of properties","caption":"Interested in buying a flat or house in Richmond?","image":"\\/media\\/carousel_1.jpg","url":"\\/for-sale"},{"title":"Rent a property","caption":"View a listing of all the homes according to your taste, wherever you want","image":"\\/media\\/carousel_2.jpg","url":"\\/for-rent"},{"title":"Our featured property","caption":"This is our featured house of the day, good value and spacious enough for a family of 5","image":"\\/media\\/carousel_3.jpg","url":"\\/property\\/4\\/lygon-place-belgravia-london-sw1w"}]', '2015-08-09 15:05:05', '2015-08-09 15:42:15');

--
-- Dumping data for table `search_criteria_types`
--

INSERT INTO `search_criteria_types` VALUES
(1, 'Houses', 'houses', NULL, 1, '2015-08-10 08:20:42', '2015-08-10 08:20:42'),
(2, 'Flats / Apartments', 'flats-apartments', NULL, 2, '2015-08-10 08:20:59', '2015-08-10 08:20:59'),
(3, 'Bungalows', 'bungalows', NULL, 3, '2015-08-10 08:21:13', '2015-08-10 08:21:13'),
(4, 'Land', 'land', NULL, 4, '2015-08-10 08:21:23', '2015-08-10 08:21:23'),
(5, 'Commercial Property', 'commercial-property', NULL, 5, '2015-08-10 08:21:30', '2015-08-10 08:21:30'),
(6, 'Other', 'other', NULL, 6, '2015-08-10 08:21:34', '2015-08-10 08:21:34');
