<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRegionTranslationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('region_translations', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('region_id')->references('id')->on('regions')->onDelete('cascade')->unsigned();
			$table->char('locale', 5)->nullable();
			$table->text('value')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('region_translations');
	}

}
