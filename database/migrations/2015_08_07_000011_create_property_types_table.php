<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePropertyTypesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('property_types', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('search_criteria_type_id')->references('id')->on('search_criteria_type')->unsigned();
			$table->string('name')->nullable();
			$table->string('handle')->nullable();
			$table->integer('position')->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('property_types');
	}

}
