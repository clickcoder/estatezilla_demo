<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePageTranslationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('page_translations', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('page_id')->references('id')->on('pages')->onDelete('cascade')->unsigned();
			$table->char('locale', 5);
			$table->string('title');
			$table->string('slug');
			$table->text('content')->nullable();
			$table->string('seo_title')->nullable();
			$table->string('seo_meta_description')->nullable();
			$table->string('seo_meta_keywords')->nullable();
			$table->enum('status', array('DRAFT','PUBLISHED'))->default('DRAFT');
			$table->enum('visibility', array('VISIBLE','HIDDEN'))->default('HIDDEN');
			$table->dateTime('published_at')->nullable()->default('0000-00-00 00:00:00');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('page_translations');
	}

}
