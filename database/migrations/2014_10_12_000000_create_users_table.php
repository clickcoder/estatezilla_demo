<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('username');
			$table->string('email')->unique();
			$table->string('password');
			$table->integer('credits')->nullable();
			$table->string('confirmation_code');
			$table->boolean('confirmed')->default(0);
			$table->enum('gender', array('male','female'))->nullable();
			$table->string('firstname')->nullable();
			$table->string('lastname')->nullable();
			$table->string('phone')->nullable();
			$table->string('mobile')->nullable();
			$table->string('fax')->nullable();
			$table->text('notes', 65535)->nullable();
			$table->boolean('is_advertiser')->default(0);
			$table->boolean('is_seller')->default(0);
			$table->boolean('is_prospect')->default(0);
			$table->boolean('is_admin')->default(0);
			$table->text('admin_permissions')->nullable();
			$table->string('ip_address')->nullable();
			$table->dateTime('validate_at')->nullable()->default('0000-00-00 00:00:00');
			$table->string('display_name')->nullable();
			$table->string('company_name')->nullable();
			$table->string('avatar')->nullable();
			$table->text('address', 65535)->nullable();
			$table->text('about', 65535)->nullable();
			$table->string('website')->nullable();
			$table->char('locale', 5)->nullable();
			$table->rememberToken();
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
