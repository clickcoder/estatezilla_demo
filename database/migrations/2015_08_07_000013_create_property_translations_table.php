<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePropertyTranslationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('property_translations', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('property_id')->references('id')->on('properties')->onDelete('cascade')->unsigned();
			$table->char('locale', 5)->nullable();
			$table->string('title')->nullable();
			$table->text('description')->nullable();
			$table->text('summary', 65535)->nullable();
			$table->text('features')->nullable();
			$table->text('photos')->nullable();
			$table->text('floor_plans')->nullable();
			$table->text('documents')->nullable();
			$table->text('virtual_tours')->nullable();
			$table->string('seo_title')->nullable();
			$table->string('seo_meta_description')->nullable();
			$table->string('seo_meta_keywords')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('property_translations');
	}

}
