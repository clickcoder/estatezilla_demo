<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePropertiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('properties', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('user_id')->references('id')->on('users')->nullable()->unsigned();
			$table->integer('property_type_id')->references('id')->on('property_types')->nullable()->unsigned();
			$table->enum('listing_type', array('sale','rent'))->default('sale');
			$table->enum('listing_status', array('for_sale','sale_under_offer','sold','to_rent','rent_under_offer','rented'))->nullable();
			$table->enum('property_condition', array('new','pre_owned','not_specified'))->default('not_specified');
			$table->decimal('property_size', 11)->nullable();
			$table->integer('num_bedrooms')->nullable();
			$table->integer('num_bathrooms')->nullable();
			$table->integer('num_floors')->nullable();
			$table->integer('num_receptions')->nullable();
			$table->boolean('has_garden')->nullable();
			$table->boolean('has_parking')->nullable();
			$table->boolean('is_investment_property')->nullable();
			$table->boolean('new_home')->nullable();
			$table->text('photos')->nullable();
			$table->text('floor_plans')->nullable();
			$table->text('documents')->nullable();
			$table->text('virtual_tours')->nullable();
			$table->string('displayable_address');
			$table->string('street_no')->nullable();
			$table->string('street_name')->nullable();
			$table->string('city')->nullable();
			$table->string('region')->nullable();
			$table->string('postcode')->nullable();
			$table->char('country', 2)->nullable();
			$table->decimal('lat', 18, 12)->nullable();
			$table->decimal('lng', 18, 12)->nullable();
			$table->decimal('price', 11)->nullable();
			$table->decimal('reduced_price', 11)->nullable();
			$table->boolean('negotiable_price')->nullable();
			$table->boolean('poa')->nullable();
			$table->string('price_period')->nullable();
			$table->string('price_type')->nullable();
			$table->boolean('is_featured')->default(0);
			$table->dateTime('expires_at')->nullable()->default('0000-00-00 00:00:00');
			$table->dateTime('published_at')->nullable()->default('0000-00-00 00:00:00');
			$table->string('origin')->nullable();
			$table->boolean('visible')->nullable()->default(0);
			$table->boolean('terms_agree')->default(0);
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('properties');
	}

}
