@extends('layouts.default')

{{-- Web site Title --}}
@section('title')
{{{ Lang::get('user/user.register') }}} ::
@parent
@stop

{{-- Content --}}
@section('content')
	<br />
	<div class="row">
		<div class="col-sm-6 col-sm-offset-3">
			<h1 class="text-center"><?= _l('Create an account') ?></h1><br />
		</div>
		<div class="col-sm-6 col-sm-offset-3">
			<div class="row">
				<div class="col-lg-12">
<div class="panel panel-default">
  <div class="panel-body">
			

					@if (count($errors) > 0)
						<div class="alert alert-danger">
							{{ ('<strong>Whoops!</strong> There were some problems with your input.') }}<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<form class="form-vertical" role="form" method="POST" action="/auth/register">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						
										<div class="form-group">
					<label ><?= _l('Name') ?></label>
					<div class="row">
						<div class="col-lg-6">
							<input type="text" class="form-control " name="first_name" placeholder="<?= _l('Enter your first name') ?>" value="{{{ old('first_name') }}}">
						</div>
						<div class="col-lg-6">
							<input type="text" class="form-control " name="last_name" placeholder="<?= _l('Enter your last name') ?>" value="{{{ old('last_name') }}}">
						</div>
					</div>
		 
				</div>
		  

				<div class="form-group">
					<label ><?= _l('Email address') ?></label>
					<input type="email" class="form-control " name="email" id="email" value="{{{ old('email') }}}" placeholder="<?= _l('Enter email') ?>">
				</div>				
				<div class="form-group">
					<label ><?= _l('Username') ?></label>
					<input type="text" class="form-control " name="username" id="username" value="{{{ old('username') }}}" placeholder="<?= _l('Enter username') ?>">
				</div>
				<div class="form-group">
					<label for="exampleInputPassword1"><?= _l('Password') ?></label>
					<input type="password" class="form-control" type="password" name="password" id="password" value="{{{ old('password') }}}" placeholder="<?= _l('Enter a password') ?>">
				</div>
				<div class="form-group">
					<label for="exampleInputPassword1"><?= _l('Confirm password') ?></label>
					<input type="password" class="form-control" type="password" name="password_confirmation" id="password_confirmation" value="{{{ old('password_confirmation') }}}" placeholder="<?= _l('Confirm your password') ?>">
				</div>							  

				<div class="checkbox">
					<label>
						<input type="checkbox"> <?= _l('We can contact you with relevant offers and news') ?>
					</label>
				</div>
				<br />
				<input type="hidden" name="coupon" value="{{{ Session::get('coupon') }}}" />
<div  class="text-center">
			  <button type="submit" class="btn btn-default btn-primary"><?= _l('Create account') ?></button>
</div>
						
					</form>
				</div>
				</div>
				</div>
				</div>
				</div>
				


</div>
<br />
<br />
<br />
<br />
@endsection
