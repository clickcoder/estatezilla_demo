@extends('layouts.user')

{{-- Web site Title --}}
@section('title')
<?= _l('Manage adverts') ?> ::
@parent
@stop

{{-- Content --}}
@section('user_area')

<div class="panel panel-default">
    <div class="panel-heading"><?= _l('Your saved properties') ?></div>
    <div class="panel-body">

        <br />

        <? if($properties->total() == 0) : ?>
        <div class="row">
            <div class="col-md-12">
                <div class="blank-slate">
                    <i class="icon-exclamation icon-4x"></i>
                    <h2><?= _l('You don\'t have any saved properties') ?></h2>
                    <p><?= _l('To save a property to your favourites use the button on the right-hand side of the property details page.') ?></p>

                </div>
            </div>
        </div>

        <? else: ?>
        <div class="row">  
            <div class="col-lg-12" >

                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th width="525"><?= _l('Property') ?></th>
                            <th><?= _l('Reference') ?></th>
                            <th><? _l('Options') ?></th>
                        </tr>
                    </thead>
                    <tbody>

                        <? foreach($properties as $property) : ?>
                        <tr>
                            <td>
                                <div class="row">
                                    <div class="col-lg-3">
                                        <a href="{{{ route_lang('property', array($property->id)) }}}" class="thumbnail">
                                            <img src="{{ $property->thumbnail('thumbs') }}" />
                                        </a>
                                    </div>
                                    <div class="col-lg-9">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <a href="{{{ route_lang('property', array($property->id)) }}}"><?= $property->title ?><br /><strong><?= _l('from') ?> <?= $property->priceFormatted ?></strong></a><br />
                                                <h6><?= $property->administrative_area_level_1 ?>, <?= $property->locality ?><br />
                                                    <?= _l('Bedrooms') ?>: <?= $property->bedrooms; ?><br />
                                                    <?= _l('Property size') ?>: <?= $property->property_size ?> <span style="">m</span><sup>2</sup></h6>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </td>
                            <td><?= str_pad($property->id, 9, '0', STR_PAD_LEFT) ?></td>
                            <td style="text-align: center">
                                <a class="btn btn-danger" href="{{{ route_lang('user/delete_favourite', array($property->id)) }}}"><?= _l('Remove') ?></a>
                            </td>
                        </tr>
                        <? endforeach; ?>



                    </tbody>
                </table>
                {{ $properties->render() }}

                <br />

            </div>
        </div>
        <? endif; ?>

    </div>
    </div>

    @stop
