@extends('layouts.default')
{{-- Web site Title --}}
@section('title')
{{{ $page_title }}} ::
@parent
@stop

{{-- Web site Title --}}
@section('description')
{{{ $page_description }}}
@parent
@stop

{{-- Content --}}
@section('content')
<ol class="breadcrumb">
  <li><a href="#">Home</a></li>
  <li><a href="#">Properties</a></li>
  <li><a href="#">For sale</a></li>
  <li class="active"><?= $property->title ?></li>
</ol>
<div class="row">
	
	
	<div class="col-xs-12">
		
		<div class="row">
			<div class="col-xs-12">

				<? if(Session::has('last_search.type')): ?>
					<br /><a href="<?= route_page(($property->listing_type == 'sale')?'for_sale':'to_rent') ?>/?<?= http_build_query(Session::get('last_search.query')) ?>#<?= $property->id ?>"><i class="fa fa-chevron-left"></i> <?= _l('back to listings') ?></a>
				<? endif; ?>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-8">
				<h3><?= $property->title ?></h3>
				<h6><?= $property->displayable_address ?></h6>
			</div>
			<div class="col-xs-4 pull-right">
				<h2 style="text-align: right"><?= $property->priceFormatted; ?></h2>
			</div>
		</div>
	</div>
	</div>
	
	<div class="row">
				
		<div class="col-xs-4 hidden-xs">
			<div class="row">
							<div class="col-xs-12">
		<div class="well" style="margin-top: 20px; padding:0; padding-right:5px; background: #000">

								
				<div id='gallery'>
					<div class='content'>
						<? foreach($property->photos as $k => $image) : ?>
							<div class="col-xs-6 col-xs-6">
								<a href="#" data-target="#carousel-example-generic" data-slide-to="<?= $k ?>" class="thumbnail text-center" data-position="<?= $k ?>" style="background: #2C3539; border-color: #000"><img alt="" src="{{ $property->singlePhoto($image->file, 'thumbs') }}" style="min-height: 70px;"/></a>
							</div>
  						<? endforeach; ?>
          </div>
          </div>
          </div>
        </div>
				

		</div>
		</div>
		
		<div class="col-sm-8 col-xs-12">
		<div class="row">
			<br />		
			
			<div class="col-xs-12">
				
				

				<? if(count($property->photos) > 0) : ?>

				<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
      <div class="carousel-inner" role="listbox">
		  <? foreach($property->photos as $i => $image) : ?>
        <div class="item <?= ($i===0)?'active':'' ?>">
          <a href="{{ $property->singlePhoto($image->file, 'full') }}" rel="slideshow" class="slideshow"><img src="{{ $property->singlePhoto($image->file, 'slideshow')  }}" /></a>
        </div>
		<? endforeach; ?>
      </div>
      <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">{{ _l('Previous') }}</span>
      </a>
      <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">{{ _l('Next') }}</span>
      </a>
</div>

	  			<? else : ?>
	  			<div class="text-center">
	  				<img class="text-center" src="{{  $property->singlePhoto('no-image', 'full') }}" style=" width: 100%;" /><br />
	  			</div>
	  			<? endif; ?>

			</div>	 
			
		</div>
		</div>

		</div>
		
				<div class="row">
				<div class="col-sm-8">

		<div class="row">
			<br />
			
			<div class="col-sm-12">
			<div class="wells">

				<div class="row">
					<div class="col-xs-12">
					<h4><?= _l('Description') ?></h4>
					@if($property_info->description != "")
						{!! nl2br($property_info->description) !!}
					@else
						<?= _l("We haven't received a description for this property") ?>
					@endif
					@if($property->reference)
					<br /><br />
					<span><?= _l('Ref code') ?> : </span><?= $property->reference ?>
					@endif
					</div>
				</div>
				
				<br />
				<h4><?= _l('Features') ?></h4>
				
				<div class="row features">
					
					<div class="col-xs-4">
						<ul>
							@if(isset($property_info->features[0]))
								<li><?= $property_info->features[0] ?></li>
							@endif
							@if(isset($property_info->features[1]))
								<li><?= $property_info->features[1] ?></li>
							@endif
							@if(isset($property_info->features[2]))
								<li><?= $property_info->features[2] ?></li>
							@endif
							@if(isset($property_info->features[9]))
								<li><?= @$property_info->features[9] ?></li>
							@endif
						</ul>
					</div>					
					<div class="col-xs-4">
						<ul>
							@if(isset($property_info->features[3]))
								<li><?= $property_info->features[3] ?></li>
							@endif
							@if(isset($property_info->features[4]))
								<li><?= $property_info->features[4] ?></li>
							@endif
							@if(isset($property_info->features[5]))
								<li><?= $property_info->features[5] ?></li>
							@endif
						</ul>
					</div>					
					<div class="col-xs-4">
						<ul>
							@if(isset($property_info->features[6]))
								<li><?= $property_info->features[6] ?></li>
							@endif
							@if(isset($property_info->features[7]))
								<li><?= $property_info->features[7] ?></li>
							@endif
							@if(isset($property_info->features[8]))
								<li><?= $property_info->features[8] ?></li>
							@endif
						</ul>
					</div>
					
				</div>
				<br />


			</div>
			</div>
		</div>		

		<div class="row">
			<div class="col-xs-12 property-pane">
					
					<ul class="nav nav-tabs" id="myTab">
						<li class="active"><a data-toggle="tab" href="#map-large"><?= _l('Map') ?></a></li>
						<li class=""><a data-toggle="tab" href="#additional-information"><?= _l('Additional information') ?></a></li>
					</ul>
					
					<div class="tab-content listing-wrapper" id="myTabContent">
						<div id="map-large" class="tab-pane fade in active">
							<br />
							<div id="map-canvas" style=" height: 350px; width: 100%; display: block; background: #000"></div>

							<script>
							function initialize() {
								var myLatlng = new google.maps.LatLng(parseFloat(<?= $property->lat ?>), parseFloat(<?= $property->lng ?>));
								var mapOptions = {
									zoom: 18,
									center: myLatlng
								};
								
								var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
								var marker = new google.maps.Marker({
									position: myLatlng,
									map: map,
									title: '<?= $property->title ?>'
								});
							}
							window.onload = initialize;
							</script>
						</div>
						<div id="additional-information" class="tab-pane fade">
							<br />
							<table class="table table-striped">
								<tbody>
								  <tr>
								    <td width="50%"><?= _l('Property type') ?></td>
								    <td><?= _l(@$property->property_type->name) ?></td>
								  </tr>							  
								  <tr>
								    <td><?= _l('Condition of property') ?></td>
								    <td><?= _l('condition.'.$property->property_condition) ?></td>
								  </tr>
								  <tr>
								    <td><?= _l('Property size') ?></td>
								    <td><?= $property->property_size ?> <?= _l('m&#178;') ?></td>
								  </tr>
								  <tr>
								    <td><?= _l('Bedrooms') ?></td>
								    <td><?= $property->num_bedrooms ?> <?= _l('bedrooms') ?></td>
								  </tr>
								  <tr>
								    <td><?= _l('Bathrooms') ?></td>
								    <td><?= $property->num_bathrooms ?> <?= _l('bathrooms') ?></td>
								  </tr>								  
								  <tr>
								    <td><?= _l('Floors') ?></td>
								    <td><?= $property->num_floors ?> <?= _l('floors') ?></td>
								  </tr>								  
								  <tr>
								    <td><?= _l('Receptions') ?></td>
								    <td><?= $property->num_receptions ?> <?= _l('receptions') ?></td>
								  </tr>
								  <tr>
								    <td><?= _l('Garden') ?></td>
								    <td><?= ($property->has_garden)?_l('Yes'):_l('No') ?></td>
								  </tr>
								  <tr>
								    <td><?= _l('Parking') ?></td>
								    <td><?= ($property->has_parking)?_l('Yes'):_l('No') ?></td>
								  </tr>
								  <tr>
								    <td><?= _l('Investment property') ?></td>
								    <td><?= ($property->is_investment_property)?_l('Yes'):_l('No') ?></td>
								  </tr>
								</tbody>
							</table>
						</div>
					</div>
					
				</div>
			</div>

			<br />

		
	</div>

	<div class="col-sm-4" id="sidebar_container">
					<div class="row">
			<div class="col-xs-12 col-sm-11 col-sm-offset-1">
		<div class="row" id="sidebar">
			<div class="col-xs-12">


		<br />
		<div id="contact_agent">
			<div class="panel panel-default">
  <div class="panel-body">

			<? if(false && $agent->agent_type == 'professional') : ?>
			<a href="#"><img alt="" src="{{ $agent->avatar() }}" /></a>
			<p><strong><?= $agent->company_name ?></strong></p>
			<p><br /><?= $agent->address ?>
			<small><br /><?= _l('Phone number') ?> : <?= $agent->phone_number ?>
			<br /><?= _l('Website') ?> : <a href="<?= $agent->website ?>" target="_blank"><?= $agent->website ?></a></small></p>
			<br />
			<? endif; ?>
			<div style="display: none" id="property_email">
				<div class="alert alert-info">
				  <?= _l('Thanks for contacting us. We\'ll get back to you shortly.') ?>
				</div>
				<h4><?=_l('What happens next?') ?></h4>
				<p style="text-align: left"><?= _l('1 - <b>We send your email to</b>') ?> <?= ($agent->company_name)?$agent->company_name:$agent->name ?><br />
				<?=_l('2 - <b>They will be in contact</b> with you as soon as possible.') ?><br /></p>
			</div>
			<?php echo Form::open(array('url' =>  URL::to('property/email'), 'class'=>'form hidden-print form ajax', 'id' => 'contact-agent-form')); ?>
				<h4 class="text-center"><?= _l('Contact this owner') ?></h4><br />
				<fieldset>
					<div class="form-group">
						<label><?= _l('Full name') ?></label>
						<?= Form::text('full_name', Input::old('full_name'), array('class' => 'form-control', 'id' => 'full_name', 'placeholder' => _l("Enter your full name") )); ?>
					</div>					
					
					<div class="form-group">
						<label><?= _l('Your phone') ?></label>
						<?= Form::text('phone_number', Input::old('phone_number'), array('class' => 'form-control', 'id' => 'phone_number', 'placeholder' => _l("Enter your phone number") )); ?>
					</div>	
					
					<div class="form-group">
						<label><?= _l('Your email') ?></label>
						<?= Form::email('your_email', Input::old('your_email'), array('class' => 'form-control', 'id' => 'your_email', 'required' => 'required', 'placeholder' => _l("Enter your email") )); ?>
					</div>		
					
					<div class="form-group">
						<label><?= _l('Your message') ?></label>
						<?= Form::textarea('message', (Input::old('message'))?Input::old('message'):_l('I would like more details about this property, please let me know when we can talk...'), array('class' => 'form-control col-xs-8', 'rows' => 3)); ?><br /><br />
					</div>
<br /><br />
					<div class="form-group text-center">
						<?= Form::hidden('property_id', $property->id); ?><br /><br />
						<button id="contact-agent" data-loading-text="<?= _l('Please wait...') ?>" class="btn btn-primary center" type="submit"><i class="icon-mail-forward"></i>  <?= _l('Contact agent') ?></button>
					</div>
				</fieldset>
			<?php echo Form::close(); ?>
			
			<div class="col-xs-12 or_call text-center">
				<span><small><?= _l('or call') ?></small> <strong><?= $property->agent->phone ?></strong></span>

			</div>
			</div>
			</div>
			</div>
			</div>

			<div class="col-xs-12 hidden-print">
				<a data-confirm-title="<?= _l("Please login") ?>" data-confirm-text="<?= _l("You need to login to be able to save a property.<br /><br />If you don't have an account please register, it only takes a minute.") ?>" data-confirm-yes="<?= _l('Login/Register') ?>" data-confirm-no="<?= _l('Cancel') ?>" data-confirm-href="{{{ route_lang('auth/login') }}}" href="<?= route_lang('favourite', array($property->id)) ?>" id="save_property" class="btn btn-default btn-block <?= (Auth::guest())?'not_logged_in':'ajax' ?>" data-replace="#save_property span" data-processing="<?= _l('Saving...') ?>"><i class="fa fa-save fa-fw pull-left"></i> <span><?= (!$favourite)?_l('Save property'):_l('Remove saved property') ?></span> </a>
				<a class="btn btn-default btn-block print-page" href="#"><i class="fa fa-print fa-fw pull-left"></i> Print </a>
				<a class="btn btn-default btn-block" href="<?= route_page('tell-friend', array($property->id)) ?>"><i class="fa fa-envelope fa-fw pull-left"></i> <?= _l('Send to friend') ?> </a>
			</div>
<?/*
			<div class="col-xs-12 pull-right hidden-print">
			<div class="row">
				<div class="col-xs-12">
					<br />
				<h4><?= _l('Share this property!') ?></h4>
				<div class="social-media-btns">
				    <a onclick="window.open(this.href,'facebookwindow', 'dependent,width=600,height=450,left='+(self.screen.availWidth/2-300)+',top='+(self.screen.availHeight/2-300) +', toolbar=0, scrollbars=0'); return false;" href="http://facebook.com/share.php?u=<?= Request::url()  ?>" title="Share this with Facebook" class="fb-share" id="facebook">
				        <i class="fa fa-facebook-official"></i>
				    </a>
				    <a onclick="window.open(this.href,'twitterwindow','dependent,width=600,height=450,left='+(self.screen.availWidth/2-300)+',top='+(self.screen.availHeight/2-300) +', toolbar=0, scrollbars=0'); return false;" href="http://twitter.com/share?text=<?= _l('Check out my property') ?> &amp;url=<?= Request::url()  ?>" title="Share this with Twitter" class="twitter-share" id="twitter">
						<i class="fa fa-twitter-square"></i>
					</a>
				    <a href="#" id="pinterest">
						<i class="fa fa-pinterest-square"></i>
					</a>

					<!-- Place this tag where you want the +1 button to render. -->
						<div class="g-plusone" data-size="tall" data-annotation="none"></div>

						<!-- Place this tag after the last +1 button tag. -->
						<script type="text/javascript">
						  (function() {
						    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
						    po.src = 'https://apis.google.com/js/platform.js';
						    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
						  })();
						</script>    
				</div>

				</div>
				</div>
				</div>
				
				*/?>
				</div>

				<div class="row">
				<div class="col-xs-12 hidden-print">
					<hr class="dull"/>
					<h4><?= _l('Want a free valuation?') ?></h4>
					<p><?= sprintf(_l('Looking to sell your property? %s for a free valuation.'), "<a href=\"".route_lang('contact')."?property_id=".$property->id."&type=report\" class=\"main-link\">" . _l('Contact us today') . "</a>"); ?></p>
				</div>

				<div class="col-xs-12 hidden-print">
					<hr class="dull"/>
					<h4><?= _l("Can't find the  ideal property") ?></h4>
					<p><?= sprintf(_l("If you can't find a property that fits your needs, %s and we'll alert you when a new property that matches your taste comes up us"), "<a href=\"".route_lang('contact')."?property_id=".$property->id."&type=feedback\" class=\"main-link\">" . _l('send us a message') . "</a>"); ?></p>
				</div>

			</div>

		</div>
		</div>
		</div>
	</div>


<script>
var main_lat = parseFloat(<?= $property->lat ?>);
var main_lng = parseFloat(<?= $property->lng ?>);
</script>
@stop