@extends('layouts.default')

{{-- Content --}}
@section('content')
<?php
	$map_locations = array();
	$map_locations[] = array(
	'lat' => 51.461311,
	'lng' => -0.303742,
	'title' => '7 bedroom house',
	'street' => 'The Vineyard, Richmond, TW10',
	'price' => '3,350,000'
	);
	$map_locations[] = array(
	'lat' => 51.451311,
	'lng' => -0.313742,
	'title' => '1 bedroom flat',
	'street' => 'Lower Mortlake Road, North Sheen',
	'price' => '229,950'
	);
	$map_locations[] = array(
	'lat' => 51.462311,
	'lng' => -0.323742,
	'title' => '2 bedroom apartment',
	'street' => 'Mount Ararat Road, Richmond',
	'price' => '520,000'
	);
	$map_locations[] = array(
	'lat' => 51.443821,
	'lng' => -0.300747,
	'title' => '2 bedroom house',
	'street' => 'The Shakespeare',
	'price' => '550,000'
	);
	$map_locations[] = array(
	'lat' => 51.452212,
	'lng' => -0.298291,
	'title' => '3 bedroom terraced house',
	'street' => 'London, TW9',
	'price' => '615,000'
	);
	$map_locations[] = array(
	'lat' => 51.466506,
	'lng' => -0.294333,
	'title' => '3 bedroom semi-detached house',
	'street' => 'Beaumont Avenue, Richmond',
	'price' => '1,100,000'
	);
	$map_locations[] = array(
	'lat' => 51.462213,
	'lng' => -0.293045,
	'title' => '2 bedroom flat',
	'street' => 'The Lodge, Courtlands, Sheen Road, Richmond',
	'price' => '399,950'
	);$map_locations[] = array(
	'lat' => 51.462161,
	'lng' => -0.292494,
	'title' => '1 bedroom flat',
	'street' => 'Sheen Road, Richmond, TW9',
	'price' => '219,950'
	);$map_locations[] = array(
	'lat' => 51.47357,
	'lng' => -0.281727,
	'title' => '5 bedroom flat',
	'street' => 'Chelwood Gardens',
	'price' => '1,250,000'
	);$map_locations[] = array(
	'lat' => 51.466213,
	'lng' => -0.293998,
	'title' => '4 bedroom house',
	'street' => 'Lower Mortlake Road, Richmond, TW9',
	'price' => '435,000'
	);
	
	
?>
<ol class="breadcrumb">
	<li><a href="#">Home</a></li>
	<li><a href="#">Properties</a></li>
	<li class="active">For sale</li>
</ol>
<div class="row">
	
	<div class="col-sm-4">
		@include('listings.map_sidebar')
	</div>
	
	<div class="col-sm-8 listings">
		<div id="ajax-loading" class="" style="display: none;">
			
			<div class="progress progress-striped active" style="width: 100%;">
		  		<div class="progress-bar"  role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
				</div>
			</div>
		</div>
		
		<script>var map_locations = <?= json_encode($map_locations); ?></script>
		<div id="map_canvas" style="width: 100%; height: 500px; "></div>
	</div>
	

</div>
<script>
var map_lat = parseFloat(<?= $map->lat ?>);
var map_lng = parseFloat(<?= $map->lng ?>);
var map_zoom = parseFloat(<?= $map->zoom ?>);
</script>
@stop
