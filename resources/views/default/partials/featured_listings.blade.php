<table class="table table-bordered table-striped">
	<thead>
		<tr>
			<th>Description</th>
			<th class="hidden-sm hidden-xs">Region</th>
			<th>Price</th>
			<th class="hidden-sm hidden-xs">Days on market</th>
			<th class="hidden-sm hidden-xs">&nbsp;</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td><a href="property.html">8 bedroom house for sale</a></td>
			<td class="hidden-sm hidden-xs">London</td>
			<td>&pound;39,950,000</td>
			<td class="hidden-sm hidden-xs">1 day</td>
			<td class="hidden-sm hidden-xs"><a href="property.html">View</a></td>
		</tr>     
		<tr>
			<td><a href="property.html">2 bedroom bungalow for sale</a></td>
			<td class="hidden-sm hidden-xs">London</td>
			<td>&pound;215,000</td>
			<td class="hidden-sm hidden-xs">3 days</td>
			<td class="hidden-sm hidden-xs"><a href="property.html">View</a></td>
		</tr>      
		<tr>
			<td><a href="property.html">3 bedroom house for sale</a></td>
			<td class="hidden-sm hidden-xs">London</td>
			<td>&pound;470,000</td>
			<td class="hidden-sm hidden-xs">2 weeks</td>
			<td class="hidden-sm hidden-xs"><a href="property.html">View</a></td>
		</tr>  	 
		<tr>
			<td><a href="property.html">3 bedroom house for rent</a></td>
			<td class="hidden-sm hidden-xs">London</td>
			<td>&pound;1,000pm</td>
			<td class="hidden-sm hidden-xs">1 month</td>
			<td class="hidden-sm hidden-xs"><a href="property.html">View</a></td>
		</tr>        
		<tr>
			<td><a href="property.html">6 bedroom house for sale</a></td>
			<td class="hidden-sm hidden-xs">London</td>
			<td>&pound;19,950,000</td>
			<td class="hidden-sm hidden-xs">2 months</td>
			<td class="hidden-sm hidden-xs"><a href="property.html">View</a></td>
		</tr>     
		<tr>
			<td><a href="property.html">5 bedroom bungalow for sale</a></td>
			<td class="hidden-sm hidden-xs">London</td>
			<td>&pound;950pm</td>
			<td class="hidden-sm hidden-xs">3 months</td>
			<td class="hidden-sm hidden-xs"><a href="property.html">View</a></td>
		</tr>      
		<tr>
			<td><a href="property.html">3 bedroom house for sale</a></td>
			<td class="hidden-sm hidden-xs">London</td>
			<td>&pound;470,000</td>
			<td class="hidden-sm hidden-xs">1 year</td>
			<td class="hidden-sm hidden-xs"><a href="property.html">View</a></td>
		</tr>  	    
		
	</tbody>
</table>